#include <stdio.h>

int main(void)
{
  int tab[6];

  tab[0] = 12;
  tab[1] = 23;
  tab[2] = 4;
  tab[3] = 75;

  printf("%d\n", tab[2]);
  tab[1] = 32;
  tab[4] = 78;
  tab[5] = 21;
  printf("%d\n", tab[1]);
  printf("%d\n", tab[4]);
  printf("%d\n", tab[5]);
}
