#include <stdio.h>

int main(void)
{
  float tab[5];
  int i = 0;
  
  tab[0] = 23;
  tab[1] = 2.62;
  tab[2] = 45;
  tab[3] = 75;
  tab[4] = 18.4;
  while (i<5) {
    printf("%.2f\n", tab[i]);
    i++;
  }
  i = 4;
  while (i >= 0) {
    printf("%.2f\n", tab[i]);
    i--;
  }
}
