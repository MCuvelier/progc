#include <stdio.h>
#include <string.h>

char    *my_revstr(char *str)
{
    int i  = 0;
    int n = 0;
    int s = 0;
    char tmp;
    
    while (str[i] != '\0')
        i = i + 1;
    n = i - 1;
    while (s < n) {
        tmp = str[s];
        str[s] = str[n];
        str[n] = tmp;
        s = s + 1;
        n = n - 1;
    }
    return (str);
}

int main(void)
{
  int nbr = 0;
  int i = 0;
  char tab[50];
 
  printf("Saisir une chaine caractères de taille 50max:\n");
  scanf("%[^\n]", &tab);
  printf("%s\n", tab);
  nbr = strlen(tab);
  printf("%s\n", my_revstr(tab));
  return (0);
}
