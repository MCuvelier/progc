#include <stdio.h>

void number(int *p1, int *p2, double *p3)
{
  int a = 0;
  int b = 0;
  double c = 0;
  
  p1 = &a;
  p2 = &b;
  p3 = &c;
}

int main(void)
{
  int p1;
  int p2;
  double p3;

  number(&p1,&p2,&p3);
  printf("p1 contient %d et son addresse %p\n",p1, &p1);
  printf("p2 contient %d et son adresse %p\n", p2, &p2);
  printf("p3 contient %.lf et son adresse %p\n", p3, p3);
  return(0);
}
