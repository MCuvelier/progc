#include <stdio.h>

int main(void) {
  puts("Ce programme affiche la taille d'éléments en mémoire");
  printf ("Soient char %d octet, int %d octets, float %d octets, double %d octets\n", sizeof(char),sizeof(int),sizeof(float),sizeof(double));
  return(0);
}
