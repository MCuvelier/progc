/*###tp2_exo3.c#################
##Donner les 5 nombres suivant##*/

#include <stdio.h>

int main(void)
{
  int nbr;
  int i = 0;
  
  printf("Saisissez un nombre : ");
  scanf("%d", &nbr);
  while (i<5) {
    i = i + 1;
    printf("%d\n", nbr + i);
  }
  return(0);
}
