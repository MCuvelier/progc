/*##tp2_exo6###############################
##Calculer le factorielle d'une nombre## */

#include <stdio.h>

int main(void)
{
  int nbr;
  int i;
  int fact = 1;
  
  printf("Saisissez un nombre : ");
  scanf("%d", &nbr);
  for (i=2; i<=nbr+1; i++) {
    fact = fact * i;
  }
  printf("%d!=%d\n",nbr, fact);
  return(0);
}
