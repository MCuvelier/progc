/*##tp2_exo5###########################
##Donner la table de multiplication## */

#include <stdio.h>

int main(void)
{
  int nbr;
  int i;
  
  printf("Saisissez un nombre : ");
  scanf("%d", &nbr);
  printf("La table de %d :\n", nbr);
  for (i=1; i<=10; i++) {
    printf("%d x %d = %d\n", nbr, i, (nbr*i));
  }
  return(0);
}
