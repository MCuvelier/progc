/*###tp2_exo1.c##################
##Ecrire un nbr supérieur à 50###*/

#include <stdio.h>

int main(void)
{
  int nbr;
  
  printf("Saisissez un nombre :");
  scanf("%d", &nbr);
  while(nbr<=50) {
    printf("Saisissez un nombre :");
    scanf("%d", &nbr);
  }
  printf("Bravo, vous avez entré un nombre supérieur à 50!\n");
  return(0);
}
