/*##tp2_exo4.c##############################
###Donner tout les nbr paires jusqu'à n## */

#include <stdio.h>

int main(void)
{
  int nbr;
  int i = 0;
  int s;
  
  printf("Saisissez un nombre : ");
  scanf("%d", &nbr);
  while (i<=nbr) {
    s = i%2;
    if (s == 0) {
      printf("%d\n", i);
      i++;
    }
    else
      i = i + 1;
  }
  return(0);
}
