/*###tp2_exo2.c##################
##Donner un nbr entre 50 et 60###*/

#include <stdio.h>

int main(void)
{
  int nbr;
  
  printf("Saisissez un nombre compris entre 50 et 60 :");
  while (nbr<=50 || nbr>=60) {
    scanf("%d", &nbr);
    if (nbr<=50)
      printf("Plus grand !");
    else if (nbr>=60)
      printf("Plus petit !");
  }
  return(0);
}
