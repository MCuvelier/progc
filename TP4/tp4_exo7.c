#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int tab(char *chaine)
{
  int compt;

  compt = strlen(chaine);
  return(compt);
}

int main(int argc, char* chaine)
{
  scanf("%[^\n]", chaine);
  printf("il y a %d caractères.\n", tab(chaine));
  return(0);
}
