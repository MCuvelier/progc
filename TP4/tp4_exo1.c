#include <stdio.h>
#include <math.h>

int main (void)
{
  float a;

  printf("entrer un nombre: ");
  scanf("%f", &a);
  printf("Son cosinus est %f\n", cos(a));
  printf("Son sinus est %f\n", sin(a));
  printf("Sa racine carré est %f\n", sqrt(a));
}
