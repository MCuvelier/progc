#include <stdio.h>

void simple_affichage_1(void)
{
  puts("Bonjour ");
}

void simple_affichage_2(int a)
{
  int i;

  for(i=0;i<a;i++)
    puts("Bonne journée");
}

int simple_affichage_3(int a)
{
  int i;
  
  for(i=0;i<a;i++) 
    puts("Bonne soirée");
  return(0);
}

int main(int nbr)
{
  nbr = 0;
  
  printf("Merci de bien vouloir donner le nombre de répétition.", stdout);
  scanf("%d", &nbr);
  simple_affichage_2(nbr);
  if (simple_affichage_3(nbr)==0)
    puts("Tout va bien");
  return(0);
}
