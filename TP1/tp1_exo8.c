#include <stdio.h>

int main(void)
{
  int nbr;
  
  printf("Entrer le nombre de kilomètre : ");
  scanf("%d", &nbr);
  if (nbr>80000 && nbr<100000)
    printf("La prime est de 800 euros\n");
  else if (nbr>=100000) 
    printf("La prime est de 1000 euros\n");
  else if (nbr<800000)
    printf("La prime est de 0 euro\n");
  return(0);
}
