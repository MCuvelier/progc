#include <stdio.h>

int main(void)
{
  float chif;
  float res;
  
  printf("Entrer un nombre :");
  scanf("%f", &chif);
  res = chif * chif * chif;
  printf("Le cube de %.2f est : %.2f\n", chif, res);
  return(0);
}
