#include <stdio.h>

int main(void)
{
  int nbr;
  
  printf("Saisissez un nombre :");
  scanf("%d", &nbr);
  printf("Le nombre %d est %s\n",nbr , (nbr%2 == 0) ? "paire" : "impaire");
  return(0);
}
