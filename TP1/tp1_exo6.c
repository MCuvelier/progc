#include <stdio.h>

int main(void)
{
  float nbr;
  
  printf("Saisissez le nombre :");
  scanf("%f", &nbr);
  if (nbr==0)
    return (0);
  printf("Le nombre %f est %s\n",nbr , (nbr>0) ? "positif" : "negatif");
  return(0);
}
