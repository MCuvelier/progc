#include <stdio.h>

int main(void)
{
  float ht;
  float tva;
  float res;
  
  printf("Saisissez le prix HT :");
  scanf("%f", &ht);
  printf("Saisissez le taux TVA :");
  scanf("%f", &tva);
  res = (tva * ht) / 100 + ht;
  printf("Le prix TTC est %.2f\n", res);
  return(0);
}
